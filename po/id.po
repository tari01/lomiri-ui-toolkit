# Indonesian translation for lomiri-ui-toolkit
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the lomiri-ui-toolkit package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-ui-toolkit\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2015-11-05 10:05+0100\n"
"PO-Revision-Date: 2014-08-31 05:21+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Indonesian <id@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Launchpad-Export-Date: 2017-04-05 07:13+0000\n"
"X-Generator: Launchpad (build 18335)\n"

#: Lomiri/Components/1.2/TextInputPopover.qml:29
#: Lomiri/Components/1.3/TextInputPopover.qml:29
msgid "Select All"
msgstr "Pilih Semua"

#: Lomiri/Components/1.2/TextInputPopover.qml:36
#: Lomiri/Components/1.3/TextInputPopover.qml:36
msgid "Cut"
msgstr "Potong"

#: Lomiri/Components/1.2/TextInputPopover.qml:48
#: Lomiri/Components/1.3/TextInputPopover.qml:48
msgid "Copy"
msgstr "Salin"

#: Lomiri/Components/1.2/TextInputPopover.qml:57
#: Lomiri/Components/1.3/TextInputPopover.qml:57
msgid "Paste"
msgstr "Tempel"

#: Lomiri/Components/1.2/ToolbarItems.qml:143
#: Lomiri/Components/1.3/ToolbarItems.qml:143
msgid "Back"
msgstr "Mundur"

#: Lomiri/Components/ListItems/1.2/Empty.qml:398
#: Lomiri/Components/ListItems/1.3/Empty.qml:398
msgid "Delete"
msgstr "Hapus"

#: Lomiri/Components/plugin/adapters/dbuspropertywatcher_p.cpp:51
msgid "No service/path specified"
msgstr "Tidak ada service/path yang ditentukan"

#: Lomiri/Components/plugin/adapters/dbuspropertywatcher_p.cpp:69
#, qt-format
msgid "Invalid bus type: %1."
msgstr "Jenis bus tidak sah: %1."

#. TRANSLATORS: Time based "this is happening/happened now"
#: Lomiri/Components/plugin/i18n.cpp:268
msgid "Now"
msgstr "Saat Ini"

#: Lomiri/Components/plugin/i18n.cpp:275
#, qt-format
msgid "%1 minute ago"
msgid_plural "%1 minutes ago"
msgstr[0] "%1 menit yang lalu"

#: Lomiri/Components/plugin/i18n.cpp:277
#, qt-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 menit"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:284
msgid "h:mm ap"
msgstr "h:mm ap"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:287
msgid "HH:mm"
msgstr "HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:293
msgid "'Yesterday 'h:mm ap"
msgstr "'Kemarin 'h:mm ap"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:296
msgid "'Yesterday 'HH:mm"
msgstr "'Kemarin 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:302
msgid "'Tomorrow 'h:mm ap"
msgstr "'Besok 'h:mm ap"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:305
msgid "'Tomorrow 'HH:mm"
msgstr "'Besok 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:312
msgid "ddd' 'h:mm ap"
msgstr "ddd' 'h:mm ap"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:315
msgid "ddd' 'HH:mm"
msgstr "ddd' 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:322
msgid "ddd d MMM' 'h:mm ap"
msgstr "ddd d MMM' 'h:mm ap"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:325
msgid "ddd d MMM' 'HH:mm"
msgstr "ddd d MMM' 'HH:mm"

#: Lomiri/Components/plugin/privates/listitemdragarea.cpp:122
msgid ""
"ListView has no ViewItems.dragUpdated() signal handler implemented. No "
"dragging will be possible."
msgstr ""
"ListView tidak memiliki signal handler terimplementasi pada "
"ViewItems.dragUpdated(). Tidak akan memungkinkan untuk melakukan Dragging."

#: Lomiri/Components/plugin/statesaverbackend_p.cpp:176
#, qt-format
msgid ""
"property \"%1\" of object %2 has type %3 and cannot be set to value \"%4\" "
"of type %5"
msgstr ""
"properti \"%1\" dari objek %2 bertipe %3 dan tak dapat diatur ke nilai "
"\"%4\" dengan tipe %5"

#: Lomiri/Components/plugin/statesaverbackend_p.cpp:185
#, qt-format
msgid "property \"%1\" does not exist or is not writable for object %2"
msgstr "properti \"%1\" tak ada atau tak dapat ditulisi untuk objek %2"

#: Lomiri/Components/plugin/ucalarm.cpp:41
#: Lomiri/Components/plugin/ucalarm.cpp:643
msgid "Alarm"
msgstr "Alarm"

#: Lomiri/Components/plugin/ucalarm.cpp:635
#: Lomiri/Components/plugin/ucalarm.cpp:667
msgid "Alarm has a pending operation."
msgstr "Alarm memiliki operasi yang tertunda"

#: Lomiri/Components/plugin/ucarguments.cpp:188
msgid "Usage: "
msgstr "Cara pakai: "

#: Lomiri/Components/plugin/ucarguments.cpp:209
msgid "Options:"
msgstr "Opsi:"

#: Lomiri/Components/plugin/ucarguments.cpp:498
#, qt-format
msgid "%1 is expecting an additional argument: %2"
msgstr "%1 mengharapkan sebuah argumen tambahan: %2"

#: Lomiri/Components/plugin/ucarguments.cpp:503
#, qt-format
msgid "%1 is expecting a value for argument: %2"
msgstr "%1 mengharapkan suatu nilai bagi argumen: %2"

#: Lomiri/Components/plugin/ucarguments.cpp:520
#, qt-format
msgid "%1 is expecting additional arguments: %2"
msgstr "%1 mengharapkan argumen-argumen tambahan: %2"

#: Lomiri/Components/plugin/uclistitemstyle.cpp:145
msgid "consider overriding swipeEvent() slot!"
msgstr "disarankan melakukan overriding pada slot swipeEvent()!"

#: Lomiri/Components/plugin/uclistitemstyle.cpp:165
msgid "consider overriding rebound() slot!"
msgstr "disarankan melakukan overriding pada slot rebound()!"

#: Lomiri/Components/plugin/ucmousefilters.cpp:1065
msgid "Ignoring AfterItem priority for InverseMouse filters."
msgstr "Mengabaikan prioritas AfterItem untuk penyaring InverseMouse."

#: Lomiri/Components/plugin/ucserviceproperties.cpp:77
msgid "Changing connection parameters forbidden."
msgstr "Tidak diperbolehkan mengubah koneksi parameter."

#: Lomiri/Components/plugin/ucserviceproperties.cpp:160
#, qt-format
msgid ""
"Binding detected on property '%1' will be removed by the service updates."
msgstr ""
"Binding terdeteksi pada properti '%1' akan dihapus dengan update service."

#: Lomiri/Components/plugin/ucstatesaver.cpp:46
msgid "Warning: attachee must have an ID. State will not be saved."
msgstr "Peringatan: lampiran mesti punya ID. Keadaan tak akan disimpan."

#: Lomiri/Components/plugin/ucstatesaver.cpp:56
#, qt-format
msgid ""
"Warning: attachee's UUID is already registered, state won't be saved: %1"
msgstr ""
"Peringatan: UUID lampiran telah terdaftar, keadaan tak akan disimpan: %1"

#: Lomiri/Components/plugin/ucstatesaver.cpp:107
#, qt-format
msgid ""
"All the parents must have an id.\n"
"State saving disabled for %1, class %2"
msgstr ""
"Semua induk mesti memiliki id.\n"
"Penyimpanan keadaan dimatikan bagi %1, kelas %2"

#: Lomiri/Components/plugin/uctheme.cpp:208
#, qt-format
msgid "Theme not found: \"%1\""
msgstr "Tema tidak ditemukan: \"%1\""

#: Lomiri/Components/plugin/uctheme.cpp:539
msgid "Not a Palette component."
msgstr "Bukan komponen Palet warna."

#: Lomiri/Components/plugin/ucviewitemsattached.cpp:462
msgid "Dragging mode requires ListView"
msgstr "Mode Dragging memerlukan ListView"

#: Lomiri/Components/plugin/ucviewitemsattached.cpp:468
msgid ""
"Dragging is only supported when using a QAbstractItemModel, ListModel or "
"list."
msgstr ""
"Dragging hanya didukung apabila menggunakan QAbstractItemModel, ListModel "
"atau list."

#: Lomiri/Components/Popups/1.2/ComposerSheet.qml:78
#: Lomiri/Components/Popups/1.3/ComposerSheet.qml:78
msgid "Cancel"
msgstr "Batal"

#: Lomiri/Components/Popups/1.2/ComposerSheet.qml:88
#: Lomiri/Components/Popups/1.3/ComposerSheet.qml:88
msgid "Confirm"
msgstr "Konfirmasi"

#: Lomiri/Components/Popups/1.2/DefaultSheet.qml:85
#: Lomiri/Components/Popups/1.3/DefaultSheet.qml:85
msgid "Close"
msgstr "Tutup"

#: Lomiri/Components/Popups/1.2/DefaultSheet.qml:95
#: Lomiri/Components/Popups/1.3/DefaultSheet.qml:95
msgid "Done"
msgstr "Selesai"

#: Lomiri/Components/Themes/Ambiance/1.2/ProgressBarStyle.qml:51
#: Lomiri/Components/Themes/Ambiance/1.3/ProgressBarStyle.qml:50
msgid "In Progress"
msgstr "Tengah Berlangsung"

#: Lomiri/Components/Themes/Ambiance/1.2/PullToRefreshStyle.qml:28
#: Lomiri/Components/Themes/Ambiance/1.3/PullToRefreshStyle.qml:28
msgid "Release to refresh..."
msgstr "Lepaskan untuk menyegarkan..."

#: Lomiri/Components/Themes/Ambiance/1.2/PullToRefreshStyle.qml:28
#: Lomiri/Components/Themes/Ambiance/1.3/PullToRefreshStyle.qml:28
msgid "Pull to refresh..."
msgstr "Tarik untuk menyegarkan..."

#~ msgid "Theme not found: "
#~ msgstr "Tema tak ditemukan: "
